//
// Created by Bartłomiej Jurek on 26-Jan-20.
//

#include "utils.h"

/**
 * Generates name for the fifo with given number.
 *
 * @param std_in Fifo's number.
 * @return generated name of the fifo.
 */
std::string generate_fifo_name(const unsigned int number) {
    // TODO: add randomness
    std::string base_name = FIFO_BNAME;
    base_name += std::to_string(number);

    return base_name;
}

/**
 * Redirects stdin stream to the given file descriptor.
 *
 * @param std_in File descriptor to which stdin will be redirected.
 */
void redirect_in(const int std_in) {
    dup2(std_in, 0);
    if (std_in != 0) close(std_in);
}

/**
 * Redirects stdout stream to the given file descriptor.
 *
 * @param std_out File descriptor to which stdout will be redirected.
 */
void redirect_out(const int std_out) {
    dup2(std_out, 1);
    if (std_out != 1) close(std_out);
}

/**
 * Redirects stdin and stdout streams to the given file descriptors.
 *
 * @param std_in File descriptor to which stdin will be redirected.
 * @param std_out File descriptor to which stdout will be redirected.
 */
void redirect_io(const int std_in, const int std_out) {
    redirect_in(std_in);
    redirect_out(std_out);
}

/**
 * Converts vector of strings to char** array.
 *
 * @param vector Vector of strings to be converted.
 * @return pointer to the converted array.
 */
char **convert_vector(const std::vector<std::string> &vector) {
    auto args = new char *[vector.size() + 1];
    for (int j = 0; j < vector.size(); ++j) {
        args[j] = new char[vector[j].size() + 1];
        strcpy(args[j], vector[j].c_str());
    }
    return args;
}

/**
 * Converts program arguments to the format expected by execvp().
 *
 * @param args Container of arguments.
 * @return arguments in format expected by execvp().
 */
char **prepare_args(const arguments &args) {
    // convert args to the type expected by execvp()
    auto prepared = convert_vector(args);
    // put nullptr at the end of argument's list as expected by execvp()
    prepared[args.size()] = nullptr;

    return prepared;
}

/**
 * Creates given number of fifos.
 *
 * @param count Number of fifos to create.
 * @return container of paths to created fifos.
 */
std::vector<std::string> create_fifos(const unsigned long count) {
    std::vector<std::string> fifos;

    for (int i = 0; i < count; ++i) {
        std::string name = FIFO_DIR + generate_fifo_name(i);
        fifos.push_back(name);
        mkfifo(name.c_str(), 0777);  // TODO: set mode
    }

    return fifos;
}

char ** get_formated_environment(std::unordered_map<std::string, std::string> env){
    auto formated_env = new char *[env.size()+1];

    int i = 0;
    for(auto iter = env.begin(); iter != env.end(); ++iter){
        std::string name =  iter->first + '=' + iter->second;
        formated_env[i] = new char[name.size() + 1];
        strcpy(formated_env[i++], name.c_str());
    }
    formated_env[i] = nullptr;
    return formated_env;
}

std::pair<std::string, std::string> get_variable_pair(char *str){
    std::string key;
    std::string value;

    char *eql_ptr = std::strchr(str, '=');

    key = std::string(str, eql_ptr);
    value = std::string(eql_ptr + 1);

    return std::make_pair(key, value);
}

void echo(arguments args){
    for(auto it=args.begin(); it != args.end(); ++it)
        std::cout << *it <<" ";
}




