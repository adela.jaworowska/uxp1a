//
// Created by Bartłomiej Jurek on 15-Dec-19.
//

#ifndef UXP1_INTERFACE_H
#define UXP1_INTERFACE_H

#include <iostream>
#include <antlr4-runtime.h>
#include "Interpreter.h"

class Interface {
private:
    std::string comm_prompt = ">";
    Interpreter* interpreter;

public:
    Interface();
    ~Interface();
    void start();
    void start_test(std::string);
    std::string get_variable(const std::string& name);
};


#endif //UXP1_INTERFACE_H
