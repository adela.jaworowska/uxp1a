#include "Interface.h"
#include "Tests.h"

using namespace antlr4;

int main() {
    Interface interface;
    interface.start();
    return 0;
}

// run functional tests
//int main(){
//
//    Tests test;
//    test.testReturnCodeEqZero();
//    test.testReturnCodeNotEqXero();
//    test.testUsername();
//    test.testChangeDir();
//    test.testWrongCommand();
//    test.exampleRun();
//}
