//
// Created by adell.j on 23.01.2020.
//
#include <assert.h>
#include <unistd.h>
#include <pwd.h>

#define GetCurrentDir getcwd
#include "Tests.h"

Tests::Tests(){
    interface = new Interface();
};

Tests::~Tests(){
    free(interface);
};

void Tests::testReturnCodeEqZero() {
    std::stringstream buffer;
    std::streambuf * old = std::cout.rdbuf(buffer.rdbuf());

    std::string command = "true";

    this->interface->start_test(command);

    std::string key = "?";
    std::string ret = this->interface->get_variable(key);
    std::cout.rdbuf(old);

    assert(ret == "0");
    std::cout << "Success: testReturnCodeEqZero" << std::endl;
}

void Tests::testReturnCodeNotEqXero() {
    std::stringstream buffer;
    std::streambuf * old = std::cout.rdbuf(buffer.rdbuf());

    std::string command = "false";

    this->interface->start_test(command);

    std::string key = "?";
    std::string ret = this->interface->get_variable(key);
    std::cout.rdbuf(old);

    assert(ret != "0");
    std::cout << "Success: testReturnCodeNotEqZero" << ret << std::endl;
}

void Tests::testCurrPath(){
    std::stringstream buffer;
    std::streambuf * old = std::cout.rdbuf(buffer.rdbuf());
//    std::cout.rdbuf(old);

    std::string command = "pwd";

    this->interface->start_test(command);
    std::string key = "PWD";
    std::string ret = this->interface->get_variable(key);
    std::cout.rdbuf(old);

    char curr_path [PATH_MAX];
    GetCurrDir(curr_path, sizeof(curr_path));

    assert(ret == curr_path);
    std::cout << "Success: testCurrPath"  << std::endl;
}

void Tests::testUsername(){
    std::stringstream buffer;
    std::streambuf * old = std::cout.rdbuf(buffer.rdbuf());

    std::string key = "USER";
    std::string ret = this->interface->get_variable(key);


    std::cout.rdbuf(old);

    struct passwd *passwd;
    passwd = getpwuid ( getuid());
    std::string username = passwd->pw_name;
    assert(ret == username);
    std::cout << "Success: testUsername"  << std::endl;
}

void Tests::testChangeDir(){
    std::stringstream buffer;
    std::streambuf * old = std::cout.rdbuf(buffer.rdbuf());
    std::string command = "cd ../..";

    this->interface->start_test(command);

    std::string key = "?";
    std::string ret = this->interface->get_variable(key);

    std::cout.rdbuf(old);

    assert(ret == "0");
    std::cout << "Success: testChangeDir" << std::endl;
}

void Tests::testWrongCommand(){
    std::stringstream buffer;
    std::streambuf * old = std::cout.rdbuf(buffer.rdbuf());
    std::string command = "wrong command";

    this->interface->start_test(command);

    std::string key = "?";
    std::string ret = this->interface->get_variable(key);

    std::cout.rdbuf(old);

    assert(ret != "0");
    std::cout << "Success: testWrongCommand" << std::endl;
}

void Tests::exampleRun(){
    std::string command = "";
    std::cout<<"Example run of program."<<std::endl;
    command = "pwd";
    std::cout<<"Command #1: '"+command+"'"<<std::endl;
    this->interface->start_test(command);
    std::cout<<"_________________________"<<std::endl;

    command = "ls";
    std::cout<<"Command #2: '"+command+"'"<<std::endl;
    this->interface->start_test(command);
    std::cout<<"_________________________"<<std::endl;

    command = "cd /tmp/";
    std::cout<<"Command #3: '"+command+"'"<<std::endl;
    this->interface->start_test(command);
    std::cout<<"_________________________"<<std::endl;

    command = "ls -la";
    std::cout<<"Command #4: '"+command+"'"<<std::endl;
    this->interface->start_test(command);
    std::cout<<"_________________________"<<std::endl;

    command = "touch file.txt";
    std::cout<<"Command #5: '"+command+"'"<<std::endl;
    this->interface->start_test(command);
    std::cout<<"_________________________"<<std::endl;

    command = "ls -la file.txt";
    std::cout<<"Command #6: '"+command+"' to check if file.txt exists"<<std::endl;
    this->interface->start_test(command);
    std::cout<<"_________________________"<<std::endl;

    command = "rm file.txt && ls -la file.txt";
    std::cout<<"Command #7: '"+command+"'"<<std::endl;
    this->interface->start_test(command);
    std::cout<<"_________________________"<<std::endl;

    command = "echo imie";
    std::cout<<"Command #8: '"+command+"'"<<std::endl;
    this->interface->start_test(command);
    std::cout<<"_________________________"<<std::endl;

    command = "imie=JAN";
    std::cout<<"Command #9: '"+command+"'"<<std::endl;
    this->interface->start_test(command);
    std::cout<<"_________________________"<<std::endl;


}