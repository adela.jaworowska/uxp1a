//
// Created by Bartłomiej Jurek on 15-Dec-19.
//

#include "Interface.h"

Interface::Interface() {
    interpreter = new Interpreter();
    // TODO: pobrac zmienne z enviromentu systemowego i zapisać do enviromentu interporetera
}

Interface::~Interface() {
    free(interpreter);
}

void Interface::start() {
    std::string comm_line;
    while (true) {
        //std::cout<<interpreter->get_curr_path()<<comm_prompt;
        std::cout << comm_prompt;
        std::getline(std::cin, comm_line);

        antlr4::ANTLRInputStream input(comm_line.c_str());
        BashLexer lexer(&input);
        antlr4::CommonTokenStream tokens(&lexer);

        tokens.fill();

        BashParser parser(&tokens);
        BashParser::Complete_commandContext *tree = parser.complete_command();

        interpreter->visit(tree);
    }
}

    void Interface::start_test(std::string command) {
            //std::cout<<interpreter->get_curr_path()<<comm_prompt;

            antlr4::ANTLRInputStream input(command);
            BashLexer lexer(&input);
            antlr4::CommonTokenStream tokens(&lexer);

            tokens.fill();

            BashParser parser(&tokens);
            BashParser::Complete_commandContext *tree = parser.complete_command();

            interpreter->visit(tree);
}

std::string Interface::get_variable(const std::string& name) {
    std::string ret_string = this->interpreter->get_shell_variable(name);
    return ret_string;
}