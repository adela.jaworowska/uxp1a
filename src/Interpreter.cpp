#include "Interpreter.h"

Interpreter::Interpreter() : BashBaseVisitor(){
    set_system_env();
    std::string initial_status_code = "0";
    set_shell_variable(("?"), initial_status_code);
}

void Interpreter::print_curr_path() {
    char curr_path [PATH_MAX];
    GetCurrDir(curr_path, sizeof(curr_path));
    std::cout<<curr_path<<std::endl;
}

antlrcpp::Any Interpreter::visitComplete_command(BashParser::Complete_commandContext *ctx) {
    if (ctx->list() != nullptr)
        return ctx->list()->accept(this);

    return antlrcpp::Any(0);
}

antlrcpp::Any Interpreter::visitList(BashParser::ListContext *ctx) {
    antlrcpp::Any status;

    for(auto& and_or: ctx->and_or()) {
        status = and_or->accept(this);
    }

    return status;
}

antlrcpp::Any Interpreter::visitAnd_or(BashParser::And_orContext *ctx) {
    auto leftResult = ctx->pipeline(0)->accept(this);

    for (int i = 0; i < ctx->AND_OR_OP().size(); i++) {
        auto operator_text = ctx->AND_OR_OP(static_cast<size_t>(i))->getText();
        if (leftResult.as<int>() != 0 && operator_text == "&&") // `false && pipeline`
            return leftResult;
        else if (leftResult.as<int>() == 0 && operator_text == "||") // `true || pipeline`
            return leftResult;

        leftResult = ctx->pipeline(static_cast<size_t>(i + 1))->accept(this); // `true  && pipeline` or `false || pipeline`
    }

    return leftResult;
}

antlrcpp::Any Interpreter::visitPipeline(BashParser::PipelineContext *ctx) {

    // TODO: add error handling for all syscalls
    mkdir(FIFO_DIR, 0777);
    // create fifo for each subsequent pair of commands in the pipeline
    auto fifos = create_fifos(ctx->command().size() - 1);

    // TODO: we should probably create a group for the pipeline; then use waitpid(-GID, ...);
    //  additionally fifo permissions could be limited to the group only
    std::vector<int> pids;
    if(ctx->command().size() == 1) {
        int stdin_copy = dup(0);
        int stdout_copy = dup(1);
        Command cmd = ctx->command(0)->accept(this).as<Command>();
        if (cmd.name == "cd") {
            int status = 0;
            if (cmd.args.size() > 1)
                 status = change_dir(cmd.args[1]);
            auto str_status = get_shell_return_code_as_string(status);
            set_shell_variable(("?"), str_status);
            return status;
        } else if(cmd.name == "export"){
            our_export(cmd.args);
            std::string str_status = "0";
            set_shell_variable(("?"), str_status);
            return 0;
        } else {
            redirect_io(stdin_copy, stdout_copy);
        }
    }
    for (int i = 0; i < ctx->command().size(); ++i) {
        int pid;
        if ((pid = fork()) == 0) {
            int std_in  = i > 0 ? open(fifos[i - 1].c_str(), O_RDONLY) : 0;
            int std_out = i < ctx->command().size() - 1 ? open(fifos[i].c_str(), O_WRONLY) : 1;
            redirect_io(std_in, std_out);

            Command cmd = ctx->command(static_cast<size_t>(i))->accept(this).as<Command>();
            auto args = prepare_args(cmd.args);
            // if cmd has just prefix (one or more assignments)
            if(ctx->command().size() == 1 && cmd.name == "") {
                return 0;
            }
            //TODO: here is the cd operation processed, change_dir method works fine, don't know why runtime error is thrown in line 93
            if(cmd.name == "cd") {
                if(cmd.args.size() > 2) {
                    std::cout << "-bash: cd: too many arguments" << std::endl;
                    exit(1);
                }
                exit(change_dir(cmd.args[1]));
            }
            else if(cmd.name == "pwd") {
                print_curr_path();
                exit(0);
            }
            else if(cmd.name == "export"){
                our_export(cmd.args);
                exit(0);
            }
            else if(cmd.name == "echo"){
                our_echo(cmd.args);
                exit(0);
            }
            else {
//                execvp(cmd.name.c_str(), args);
                auto env = get_formated_environment(cmd.environment);
                execvpe(cmd.name.c_str(), args, env);
            }
        } else {
            pids.push_back(pid);
        }
    }

    // wait for the last command to finish and get it's return status
    int status;
    waitpid(pids.back(), &status, 0);
    // wait until the other processes are finished; discard return statuses
    for (int i = 0; i < pids.size() - 1; ++i) {
        waitpid(pids[i], nullptr, 0);
    }
    auto status_str = get_shell_return_code_as_string(status);
    set_shell_variable(("?"), status_str);

    int returned = WEXITSTATUS(status);

    if (!WIFEXITED(status) ) {
        if (WIFEXITED(status)) {
            printf("exited, status=%d\n", WEXITSTATUS(status));
        } else if (WIFSIGNALED(status)) {
            printf("killed by signal %d\n", WTERMSIG(status));
        } else if (WIFSTOPPED(status)) {
            printf("stopped by signal %d\n", WSTOPSIG(status));
        } else if (WIFCONTINUED(status)) {
            printf("continued\n");
        }
        return returned;
    }

    // remove fifos
    for (auto &fifo: fifos) {
        unlink(fifo.c_str());
    }
    unlink(FIFO_DIR);
    
    return returned;
}

int Interpreter::change_dir(std::string path) {
    if(chdir(path.c_str()) == -1) {
        std::cout<<"Error Description: "<<strerror(errno)<<std::endl;
        return 1;
    }
    return 0;
}

void Interpreter::set_system_env() {
    auto iter = environ;
    while(*(iter) != nullptr) {
        auto pair = get_variable_pair(*iter);
        set_env_variable(pair.first, pair.second);
        iter++;
    }
}

void Interpreter::set_env_variable(const std::string& key, std::string& value){
    execution_environment.insert_or_assign(key, value);
    shell_variables.insert_or_assign(key, value);
}

std::string Interpreter::get_env_variable(const std::string& key){
    try {
        return execution_environment.at(key);
    }
    catch(const std::out_of_range &e) {
        return "";
    }
}

void Interpreter::set_shell_variable(const std::string& key, std::string& value){
    if(execution_environment.find(key) != execution_environment.end()) {
        set_env_variable(key, value);
    }
    shell_variables.insert_or_assign(key, value);
}

std::string Interpreter::get_shell_return_code_as_string(int& status){
    int status_ret_code = status % 255;
    std::string status_str = std::to_string(status_ret_code);
    return status_str;
}

std::string Interpreter::get_shell_variable(const std::string& key){
    try {
        return shell_variables.at(key);
    }
    catch(const std::out_of_range &e) {
        return "";
    }
}

void Interpreter::our_export(arguments& args){
    for(auto &arg : args) {
        std::string key;
        std::string value;

        auto index = arg.find('=');
        if (index != std::string::npos) {
            key = arg.substr(0, index);
            value = arg.substr(index + 1);
        } else {
            key = std::string(arg);
            value = get_shell_variable(key);
        }

        set_env_variable(key, value);
    }
}

void Interpreter::our_echo(arguments args){
    for(int i=1; i < args.size(); i++) {
        std::cout << args[i] << " ";
    }
    std::cout << std::endl;
}

std::string Interpreter::parse_string(const std::string& arg, char curr_quote) { //this function parse string detecting quotes and variables
    std::string result = "";
    int i = 0;
    while(i < arg.length()) {
        std::string tmp = "";
        char next_char = arg[i++];  //get curr char and increment counter
        if(next_char == '\'' || next_char == '"') {
            if(curr_quote == next_char) {
                curr_quote = 0; // we've found the ending quote
                continue;
            } else if(curr_quote == 0) {
                result += parse_string(arg.substr(i, arg.length() - i), next_char);
                i = arg.length(); //to end the loop
            } else {
                result += next_char; //it's for sure single in double quotes or double in single quotes
            }
        } else if (next_char == '$' && curr_quote != '\'') {
            std::string variable_name = "";
            while(i < arg.length() && arg[i] != '\'' && arg[i] != '"' && arg[i] != '$' && arg[i] != ' ') {
                variable_name += arg[i++];
            }
            result += get_shell_variable(variable_name);
        } else {
            result += next_char;
        }
    }
    return result;
}

antlrcpp::Any Interpreter::visitCommand(BashParser::CommandContext *ctx) {
    Command cmd = {};
    cmd.environment = execution_environment;

    std::vector<std::pair<std::string, std::string>> assignments;

    if (ctx->cmd_prefix() != nullptr) {
        assignments = ctx->cmd_prefix()->accept(this).as<std::vector<std::pair<std::string, std::string>>>();
    }

    if (ctx->cmd_name() != nullptr) {
        cmd.name = ctx->cmd_name()->accept(this).as<std::string>();
        cmd.args.push_back(cmd.name); // first argument should be the program name
        for (int i = 0; i < assignments.size(); ++i) {
            cmd.environment.insert(std::make_pair(assignments[i].first, assignments[i].second));
        }
    } else {
        for (auto &assignment : assignments) {
            set_shell_variable(assignment.first, assignment.second);
        }
    }

    if (ctx->cmd_suffix() != nullptr) {
        auto args = ctx->cmd_suffix()->accept(this).as<arguments>();
        cmd.args.insert(
            cmd.args.end(),
            std::make_move_iterator(args.begin()),
            std::make_move_iterator(args.end())
        );
    }

    return antlrcpp::Any(cmd);
}

antlrcpp::Any Interpreter::visitCmd_name(BashParser::Cmd_nameContext *ctx) {
    return antlrcpp::Any(parse_string(ctx->WORD()->getText()));
}

antlrcpp::Any Interpreter::visitCmd_prefix(BashParser::Cmd_prefixContext *ctx) {
    std::vector<std::pair<std::string, std::string>> assignments;

    for (int i = 0; i < ctx->io_redirect().size(); ++i) {
        ctx->io_redirect(static_cast<size_t>(i))->accept(this);
    }

    for (int i = 0; i < ctx->assignment_word().size(); ++i) {
        assignments.push_back(ctx->assignment_word(static_cast<size_t>(i))->accept(this));
    }

    return antlrcpp::Any(assignments);
}

antlrcpp::Any Interpreter::visitCmd_suffix(BashParser::Cmd_suffixContext *ctx) {
    arguments args;

    for (int i = 0; i < ctx->WORD().size(); ++i) {
        args.push_back(parse_string(ctx->WORD(static_cast<size_t>(i))->getText()));
    }

    for (int i = 0; i < ctx->io_redirect().size(); ++i) {
        ctx->io_redirect(static_cast<size_t>(i))->accept(this);
    }

    return antlrcpp::Any(args);
}

antlrcpp::Any Interpreter::visitIo_redirect_in(BashParser::Io_redirect_inContext *ctx) {
    auto filename = ctx->filename()->accept(this).as<std::string>();

    if(access(filename.c_str(), R_OK) != 0) {
        printf("Error Number : %d\n", errno);
        perror("Error Description: ");
    }

    auto fd = open(filename.c_str(), O_RDONLY);
    redirect_in(fd);

    return antlrcpp::Any();
}

antlrcpp::Any Interpreter::visitIo_redirect_out(BashParser::Io_redirect_outContext *ctx) {
    auto filename = ctx->filename()->accept(this).as<std::string>();
    auto fd = open(filename.c_str(), O_WRONLY | O_CREAT, 0755);
    redirect_out(fd);

    return antlrcpp::Any();
}

antlrcpp::Any Interpreter::visitFilename(BashParser::FilenameContext *ctx) {
    return antlrcpp::Any(parse_string(ctx->WORD()->getText()));
}

antlrcpp::Any Interpreter::visitAssignment_word(BashParser::Assignment_wordContext *ctx) {
    auto key = ctx->assignment_key()->accept(this).as<std::string>();
    auto value = ctx->assignment_value()->accept(this).as<std::string>();

    std::pair<std::string, std::string> pair(key, value);

    return antlrcpp::Any(pair);
}

antlrcpp::Any Interpreter::visitAssignment_key(BashParser::Assignment_keyContext *ctx) {
    return antlrcpp::Any(ctx->getText());
}

antlrcpp::Any Interpreter::visitAssignment_value(BashParser::Assignment_valueContext *ctx) {
    return antlrcpp::Any(parse_string(ctx->WORD()->getText())); //calling method which build fulls string detecting quotes and variables, echo should use it also
}