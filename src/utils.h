#pragma once

#ifndef UXP1_UTILS_H
#define UXP1_UTILS_H

#include <sys/stat.h>
#include <string>
#include <unistd.h>
#include <cstring>
#include <list>
#include <vector>
#include <unordered_map>
#include <iostream>

static const char *const FIFO_DIR = "/tmp/bash/";
static const char *const FIFO_BNAME = "fifo_";

typedef std::vector<std::string> arguments;

std::string generate_fifo_name(const unsigned int number);

void redirect_in(const int std_in);

void redirect_out(const int std_out);

void redirect_io(const int std_in, const int std_out);

char **convert_vector(const std::vector<std::string> &vector);

char **prepare_args(const arguments &args);

std::vector<std::string> create_fifos(const unsigned long count);

char ** get_formated_environment(std::unordered_map<std::string, std::string> env);

std::pair<std::string, std::string> get_variable_pair(char *str);

void echo(arguments args);

#endif //UXP1_UTILS_H
