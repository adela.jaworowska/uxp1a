#pragma once

#ifdef WINDOWS
    #include <direct.h>
    #define GetCurrDir _getcwd
#else
    #include <unistd.h>
    #define GetCurrDir getcwd
#endif

#include <antlr4-runtime.h>
#include <BashLexer.h>
#include <BashParser.h>
#include <BashBaseVisitor.h>
#include <unordered_map>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <vector>
#include <iostream>
#include <limits.h>
#include "Command.h"
#include "utils.h"

using namespace bashGrammar;

class Interpreter : public BashBaseVisitor {
private:
    int change_dir(std::string path);
    std::unordered_map<std::string, std::string> execution_environment; // environment for global variables
    std::unordered_map<std::string, std::string> shell_variables; // shell variables not inherited by child processes

public:

    Interpreter();

    void print_curr_path();

    void set_system_env();

    void set_env_variable(const std::string& key, std::string& value);

    std::string get_env_variable(const std::string& key);

    void set_shell_variable(const std::string& key, std::string& value);

    std::string get_shell_variable(const std::string& key);

    void our_export(arguments& args);

    void our_echo(arguments args);

    std::string get_shell_return_code_as_string(int& status);

    std::string parse_string(const std::string& arg, char curr_quote = 0);

    antlrcpp::Any visitComplete_command(BashParser::Complete_commandContext *ctx) override;

    antlrcpp::Any visitList(BashParser::ListContext *ctx) override;

    antlrcpp::Any visitAnd_or(BashParser::And_orContext *ctx) override;

    antlrcpp::Any visitPipeline(BashParser::PipelineContext *ctx) override;

    antlrcpp::Any visitCommand(BashParser::CommandContext *ctx) override;

    antlrcpp::Any visitCmd_name(BashParser::Cmd_nameContext *ctx) override;

    antlrcpp::Any visitCmd_prefix(BashParser::Cmd_prefixContext *ctx) override;

    antlrcpp::Any visitCmd_suffix(BashParser::Cmd_suffixContext *ctx) override;

    antlrcpp::Any visitIo_redirect_in(BashParser::Io_redirect_inContext *ctx) override;

    antlrcpp::Any visitIo_redirect_out(BashParser::Io_redirect_outContext *ctx) override;

    antlrcpp::Any visitFilename(BashParser::FilenameContext *ctx) override;

    antlrcpp::Any visitAssignment_word(BashParser::Assignment_wordContext *ctx) override;

    antlrcpp::Any visitAssignment_key(BashParser::Assignment_keyContext *ctx) override;

    antlrcpp::Any visitAssignment_value(BashParser::Assignment_valueContext *ctx) override;

};