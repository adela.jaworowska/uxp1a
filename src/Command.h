#pragma once
#ifndef UXP1_COMMAND_H
#define UXP1_COMMAND_H

#include <string>
#include "utils.h"

struct Command {
    std::string name;
    arguments args;
    std::unordered_map<std::string, std::string> environment;
};

#endif //UXP1_COMMAND_H
