//
// Created by adell.j on 23.01.2020.
//

#ifndef UXP1_TESTS_H
#define UXP1_TESTS_H
#include <iostream>
#include <antlr4-runtime.h>
#include "Interface.h"



class Tests {
private:
    Interface* interface;
    public:
    Tests();
    ~Tests();
    void testReturnCodeEqZero();
    void testReturnCodeNotEqXero();
    void testCurrPath();
    void testUsername();
    void testChangeDir();
    void testWrongCommand();
    void exampleRun();
};


#endif //UXP1_TESTS_H
