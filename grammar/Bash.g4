grammar Bash;

complete_command
    : list?
    ;

list
    : and_or (SEPARATOR_OP and_or)* SEPARATOR_OP?
    ;

and_or
    : pipeline (AND_OR_OP pipeline)*
    ;

pipeline
    : command (PIPE command)*
    ;

command
    : cmd_prefix cmd_name cmd_suffix 
    | cmd_prefix cmd_name 
    | cmd_prefix 
    | cmd_name cmd_suffix 
    | cmd_name
    ;

cmd_name
    : WORD
    ;

cmd_prefix
    : (io_redirect|assignment_word)+
    ;

cmd_suffix
    : (io_redirect|WORD)+
    ;

io_redirect
    : LESS_THEN filename     #Io_redirect_in
    | GREATER_THEN filename  #Io_redirect_out
    ;

filename
    : WORD
    ;

assignment_word
    : assignment_key EQ assignment_value
    ;
    
assignment_key
    : WORD
    ;

assignment_value
    : WORD
    ;

GREATER_THEN
    : '>'
    ;

LESS_THEN
    : '<'
    ;

AND_OR_OP
    : AND_AND
    | OR_OR
    ;

AND_AND
    : '&&'
    ;
    
OR_OR
    : '||'
    ;

SEPARATOR_OP
    : ';'
    ;

//ASSIGNMENTWORD
//    : (LETTER | '_') WORD*
//    ;

WORD
    : '"' (NORMAL_WORD | QUOTED_TOKENS | '\'' | ' ')+ '"'
    | '\'' (NORMAL_WORD | QUOTED_TOKENS | '"' | ' ')+ '\''
    | NORMAL_WORD
    ;

QUOTED_TOKENS
    : '$'LETTER(LETTER|DIGIT|TOKEN_ALLOWED_CHARS)* // TODO: fill with all needed; see https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_02
    ;

NORMAL_WORD
    : (LETTER|DIGIT|ALLOWED_CHARS)+
    ;

fragment ALLOWED_CHARS
    : [._\-+/!@#$%^&*(){}[\]<>?:~,] // TODO: add ; char?; check with https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html
    ;

fragment TOKEN_ALLOWED_CHARS
    : [a-zA-Z_]+[a-zA-Z0-9_]*
    ;

fragment LETTER
    : [a-zA-Z]
    ;

fragment DIGIT
    : [0-9]
    ;

EQ
    : '='
    ;

PIPE
    : '|'
    ;

WHITESPACE
    : [ \t\n\r]+ -> skip // skip is command for antler
    ;
